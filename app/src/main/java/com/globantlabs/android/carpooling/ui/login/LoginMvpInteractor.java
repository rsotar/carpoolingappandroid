package com.globantlabs.android.carpooling.ui.login;

import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.ui.base.MvpInteractor;

import io.reactivex.Single;
import retrofit2.Call;

public interface LoginMvpInteractor extends MvpInteractor {

    Call<LoginResponse> getLoginCall(LoginRequest loginRequest);

    Call<ProfileResponse> getProfileCall(String accessToken);
}
