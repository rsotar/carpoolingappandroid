package com.globantlabs.android.carpooling.ui.register;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.globantlabs.android.carpooling.R;
import com.globantlabs.android.carpooling.ui.SecondActivity;
import com.globantlabs.android.carpooling.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterMvpView {

    @Inject
    RegisterMvpPresenter<RegisterMvpView, RegisterMvpInteractor> presenter;
    @BindView(R.id.btn_register_continue)
    Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));
        presenter.onAttach(RegisterActivity.this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_register_continue)
    public void onViewClicked() {
        startActivity(new Intent(getBaseContext(), SecondActivity.class));
    }
}