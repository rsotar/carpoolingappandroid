package com.globantlabs.android.carpooling.data.prefs;


import android.content.Context;
import android.content.SharedPreferences;

import com.globantlabs.android.carpooling.di.ApplicationContext;
import com.globantlabs.android.carpooling.di.PreferenceInfo;
import com.globantlabs.android.carpooling.utils.AppConstants;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_USER_LAST_NAME = "PREF_KEY_USER_LAST_NAME";
    private static final String PREF_KEY_PICTURE_URL = "PREF_KEY_PICTURE_URL";
    private static final String PREF_KEY_USER_SITE = "PREF_KEY_USER_SITE";
    private static final String PREF_KEY_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_ADDRESS = "PREF_KEY_ADDRESS";
    private static final String PREF_KEY_DRIVER = "PREF_KEY_DRIVER";
    private static final String PREF_KEY_STATUS = "PREF_KEY_STATUS";
    private static final String PREF_KEY_USER_STATUS = "PREF_KEY_USER_STATUS";
    private static final String PREF_KEY_LAST_ACTION = "PREF_KEY_LAST_ACTION";

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_REFRESH_TOKEN = "PREF_KEY_REFRESH_TOKEN";

    private final SharedPreferences sharedPreferences;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        sharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }


    @Override
    public String getUserName() {
        return sharedPreferences.getString(PREF_KEY_USER_NAME, null);
    }

    @Override
    public void setUserName(String userName) {
        sharedPreferences.edit().putString(PREF_KEY_USER_NAME, userName).apply();
    }

    @Override
    public String getUserLastName() {
        return sharedPreferences.getString(PREF_KEY_USER_LAST_NAME, null);
    }

    @Override
    public void setUserLastName(String userName) {
        sharedPreferences.edit().putString(PREF_KEY_USER_LAST_NAME, userName).apply();
    }

    @Override
    public String getPictureUrl() {
        return sharedPreferences.getString(PREF_KEY_PICTURE_URL, null);
    }

    @Override
    public void setPictureUrl(String pictureUrl) {
        sharedPreferences.edit().putString(PREF_KEY_PICTURE_URL, pictureUrl).apply();
    }

    @Override
    public String getUserSite() {
        return sharedPreferences.getString(PREF_KEY_USER_SITE, null);
    }

    @Override
    public void setUserSite(String site) {
        sharedPreferences.edit().putString(PREF_KEY_USER_SITE, site).apply();
    }

    @Override
    public String getUserEmail() {
        return sharedPreferences.getString(PREF_KEY_USER_EMAIL, null);
    }

    @Override
    public void setUserEmail(String email) {
        sharedPreferences.edit().putString(PREF_KEY_USER_EMAIL, email).apply();
    }

    @Override
    public String getAddress() {
        return sharedPreferences.getString(PREF_KEY_ADDRESS, null);
    }

    @Override
    public void setAddress(String address) {
        sharedPreferences.edit().putString(PREF_KEY_ADDRESS, address).apply();
    }

    @Override
    public boolean getIsDriver() {
        return sharedPreferences.getBoolean(PREF_KEY_DRIVER, false);
    }

    @Override
    public void setIsDriver(boolean driver) {
        sharedPreferences.edit().putBoolean(PREF_KEY_ADDRESS, driver).apply();
    }

    @Override
    public String getStatus() {
        return sharedPreferences.getString(PREF_KEY_STATUS, null);
    }

    @Override
    public void setStatus(String status) {
        sharedPreferences.edit().putString(PREF_KEY_STATUS, status).apply();
    }

    @Override
    public String getUserStatus() {
        return sharedPreferences.getString(PREF_KEY_USER_STATUS, null);
    }

    @Override
    public void setUserStatus(String userStatus) {
        sharedPreferences.edit().putString(PREF_KEY_USER_STATUS, userStatus).apply();
    }

    @Override
    public String getLastAction() {
        return sharedPreferences.getString(PREF_KEY_LAST_ACTION, null);
    }

    @Override
    public void setLastAction(String lastAction) {
        sharedPreferences.edit().putString(PREF_KEY_LAST_ACTION, lastAction).apply();
    }


    @Override
    public int getUserLoggedInMode() {
        return sharedPreferences.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setUserLoggedInMode(AppConstants.LoggedInMode mode) {
        sharedPreferences.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getAccessToken() {
        return sharedPreferences.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        sharedPreferences.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getRefreshToken() {
        return sharedPreferences.getString(PREF_KEY_REFRESH_TOKEN, null);
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        sharedPreferences.edit().putString(PREF_KEY_REFRESH_TOKEN, refreshToken).apply();
    }
}
