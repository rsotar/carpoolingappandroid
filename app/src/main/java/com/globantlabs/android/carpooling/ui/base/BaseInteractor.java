package com.globantlabs.android.carpooling.ui.base;


import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.utils.AppConstants;

import javax.inject.Inject;

public class BaseInteractor implements MvpInteractor {

    private final PreferencesHelper preferencesHelper;
    private final ApiHelper apiHelper;

    @Inject
    public BaseInteractor(PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        this.preferencesHelper = preferencesHelper;
        this.apiHelper = apiHelper;
    }

    @Override
    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }

    @Override
    public void setAccessToken(String accessToken) {
        getPreferencesHelper().setAccessToken(accessToken);

    }

    @Override
    public void updateUserTokens(String accessToken, String refreshToken){
        getPreferencesHelper().setAccessToken(accessToken);
        getPreferencesHelper().setRefreshToken(refreshToken);
    }

    @Override
    public void updateUserInfo(
            ProfileResponse response,
            AppConstants.LoggedInMode loggedInMode) {

        getPreferencesHelper().setUserName(response.getName());
        getPreferencesHelper().setUserLastName(response.getLastName());
        getPreferencesHelper().setPictureUrl(response.getPictureUrl());
        getPreferencesHelper().setUserSite(response.getSite());
        getPreferencesHelper().setUserEmail(response.getEmail());
        getPreferencesHelper().setAddress(response.getAddress());
        getPreferencesHelper().setIsDriver(response.getIsDriver());
        getPreferencesHelper().setStatus(response.getStatus());
        getPreferencesHelper().setUserStatus(response.getUserStatus());
        getPreferencesHelper().setLastAction(response.getLastAction());

        getPreferencesHelper().setUserLoggedInMode(loggedInMode);
    }

    @Override
    public void setUserAsLoggedOut() {
        getPreferencesHelper().setAccessToken(null);

        getPreferencesHelper().setUserName(null);
        getPreferencesHelper().setUserLastName(null);
        getPreferencesHelper().setPictureUrl(null);
        getPreferencesHelper().setUserSite(null);
        getPreferencesHelper().setUserEmail(null);
        getPreferencesHelper().setAddress(null);
        getPreferencesHelper().setIsDriver(false);
        getPreferencesHelper().setStatus(null);
        getPreferencesHelper().setUserStatus(null);
        getPreferencesHelper().setLastAction(null);

        getPreferencesHelper().setUserLoggedInMode(AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
    }

}
