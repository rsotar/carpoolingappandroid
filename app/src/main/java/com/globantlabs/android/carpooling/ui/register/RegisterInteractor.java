package com.globantlabs.android.carpooling.ui.register;


import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.ui.base.BaseInteractor;

import javax.inject.Inject;

public class RegisterInteractor extends BaseInteractor
        implements RegisterMvpInteractor {

    @Inject
    public RegisterInteractor(PreferencesHelper preferencesHelper,
                              ApiHelper apiHelper) {

        super(preferencesHelper, apiHelper);
    }

}