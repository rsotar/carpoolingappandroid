package com.globantlabs.android.carpooling.data.network;

import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiHelper {

    @POST("/user/login")
    Call<LoginResponse> getLoginCall(@Body LoginRequest loginRequest);

    @GET("/user/profile")
    Call<ProfileResponse> getProfileCall(@Header("Authorization") String accessToken);
}
