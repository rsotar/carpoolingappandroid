package com.globantlabs.android.carpooling.ui.login;


import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.ui.base.BasePresenter;
import com.globantlabs.android.carpooling.utils.AppLogger;
import com.globantlabs.android.carpooling.utils.rx.SchedulerProvider;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginPresenter<V extends LoginMvpView, I extends LoginMvpInteractor>
        extends BasePresenter<V, I> implements LoginMvpPresenter<V, I> {

    private static final String TAG = "LoginPresenter";
    private I interactor;
    private V view;

    @Inject
    public LoginPresenter(I mvpInteractor,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
        interactor = getInteractor();
        view = getMvpView();
    }


    @Override
    public void onGoogleLoginClick() {
        getMvpView().openGoogleSignInActivity();
    }

    @Override
    public void userIsAlreadyLogged() {
        getMvpView().openRegisterActivity();
    }

    @Override
    public void singInResultReceived(GoogleSignInResult result) {
        AppLogger.d("singInResultReceived: " + result.getStatus().toString());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            doLoginRequest(acct);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    private void doLoginRequest(GoogleSignInAccount account) {
        String email = account.getEmail();
        String authCode = account.getServerAuthCode();
        AppLogger.d("Sending | Email: " + email + " - authCode: " + authCode);

        view.showLoading();

        LoginRequest request = new LoginRequest(email, authCode);

        interactor.getLoginCall(request).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                view.hideLoading();

                if(response != null && response.body() != null){
                    response.body();
                }else {
                    view.showMessage("Error al iniciar sesion. Codigo: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (!isViewAttached()) {
                    return;
                }
                view.hideLoading();
                view.showMessage(t.getMessage());
            }
        });

    }

    private void doProfileRequest(String accessToken) {
        AppLogger.d("Sending | accessToken: " + accessToken);

        interactor.getProfileCall("bearer " + accessToken).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                response.body();
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                t.getMessage();
            }
        });
    }
}
