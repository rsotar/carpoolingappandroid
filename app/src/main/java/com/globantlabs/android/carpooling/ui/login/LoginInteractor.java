package com.globantlabs.android.carpooling.ui.login;

import com.globantlabs.android.carpooling.data.network.ApiHelper;
import com.globantlabs.android.carpooling.data.network.model.LoginRequest;
import com.globantlabs.android.carpooling.data.network.model.LoginResponse;
import com.globantlabs.android.carpooling.data.network.model.ProfileResponse;
import com.globantlabs.android.carpooling.data.prefs.PreferencesHelper;
import com.globantlabs.android.carpooling.ui.base.BaseInteractor;

import javax.inject.Inject;

import retrofit2.Call;

public class LoginInteractor extends BaseInteractor
        implements LoginMvpInteractor {

    @Inject
    public LoginInteractor(PreferencesHelper preferencesHelper,
                           ApiHelper apiHelper) {
        super(preferencesHelper, apiHelper);
    }

    @Override
    public Call<LoginResponse> getLoginCall(LoginRequest loginRequest) {
        return getApiHelper().getLoginCall(loginRequest);
    }

    @Override
    public Call<ProfileResponse> getProfileCall(String accessToken) {
        return getApiHelper().getProfileCall(accessToken);
    }
}
