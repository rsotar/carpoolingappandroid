package com.globantlabs.android.carpooling;

import android.app.Application;

import com.globantlabs.android.carpooling.di.component.ApplicationComponent;
import com.globantlabs.android.carpooling.di.component.DaggerApplicationComponent;
import com.globantlabs.android.carpooling.di.module.ApplicationModule;
import com.globantlabs.android.carpooling.di.module.NetworkModule;
import com.globantlabs.android.carpooling.utils.AppLogger;


public class CarPoolingApp extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(BuildConfig.BASE_URL))
                .build();

        applicationComponent.inject(this);

        AppLogger.init();

    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }
}
