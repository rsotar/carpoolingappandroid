package com.globantlabs.android.carpooling.data.network.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProfileResponseTest {

    private final String NAME = "NAME";
    private final String LAST_NAME = "LAST_NAME";
    private final String PICTURE_URL = "PICTURE_URL";
    private final String SITE = "SITE";
    private final String EMAIL = "EMAIL";
    private final String ADDRESS = "ADDRESS";
    private final boolean DRIVER = true;
    private final String STATUS = "STATUS";
    private final String USER_STATUS = "USER_STATUS";
    private final String LAST_ACTION = "LAST_ACTION";
    private ProfileResponse profileResponse;

    @Before
    public void setUp(){
        profileResponse = new ProfileResponse();
    }

    @Test
    public void testNameAccessors(){
        profileResponse.setName(NAME);
        assertThat(profileResponse.getName(), is(NAME));
    }

    @Test
    public void testLastNameAccessors(){
        profileResponse.setLastName(LAST_NAME);
        assertThat(profileResponse.getLastName(), is(LAST_NAME));
    }

    @Test
    public void testPictureUrlAccessors(){
        profileResponse.setPictureUrl(PICTURE_URL);
        assertThat(profileResponse.getPictureUrl(), is(PICTURE_URL));
    }

    @Test
    public void testSiteAccessors(){
        profileResponse.setSite(SITE);
        assertThat(profileResponse.getSite(), is(SITE));
    }

    @Test
    public void testEmailAccessors(){
        profileResponse.setEmail(EMAIL);
        assertThat(profileResponse.getEmail(), is(EMAIL));
    }

    @Test
    public void testAddressAccessors(){
        profileResponse.setAddress(ADDRESS);
        assertThat(profileResponse.getAddress(), is(ADDRESS));
    }

    @Test
    public void testDriverAccessors(){
        profileResponse.setIsDriver(DRIVER);
        assertThat(profileResponse.getIsDriver(), is(DRIVER));
    }

    @Test
    public void testStatusAccessors(){
        profileResponse.setStatus(STATUS);
        assertThat(profileResponse.getStatus(), is(STATUS));
    }

    @Test
    public void testUserStatusAccessors(){
        profileResponse.setUserStatus(USER_STATUS);
        assertThat(profileResponse.getUserStatus(), is(USER_STATUS));
    }

    @Test
    public void testLastActionAccessors(){
        profileResponse.setLastAction(LAST_ACTION);
        assertThat(profileResponse.getLastAction(), is(LAST_ACTION));
    }
}

