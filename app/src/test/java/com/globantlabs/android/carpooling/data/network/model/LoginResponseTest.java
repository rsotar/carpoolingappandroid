package com.globantlabs.android.carpooling.data.network.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LoginResponseTest {
    private final String ACCESS_TOKEN = "accessToken";
    private final String REFRESH_TOKEN = "refreshToken";
    private LoginResponse loginResponse;

    @Before
    public void setUp(){
        loginResponse = new LoginResponse();
    }

    @Test
    public void testAccessTokenAccessors(){
        loginResponse.setAccessToken(ACCESS_TOKEN);
        assertThat(loginResponse.getAccessToken(), is(ACCESS_TOKEN));
    }


    @Test
    public void testRefreshTokenAccessors(){
        loginResponse.setRefreshToken(REFRESH_TOKEN);
        assertThat(loginResponse.getRefreshToken(), is(REFRESH_TOKEN));
    }
}
