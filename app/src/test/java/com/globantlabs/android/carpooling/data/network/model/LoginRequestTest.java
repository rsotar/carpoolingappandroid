package com.globantlabs.android.carpooling.data.network.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LoginRequestTest {
    private final String EMAIL = "user@mail.com";
    private final String TOKEN = "googleToken";
    private LoginRequest request;

    @Before
    public void setUp(){
        request = new LoginRequest(EMAIL, TOKEN);
    }

    @Test
    public void testLoginRequestConstructor(){
        assertThat(request != null, is(true));
    }

    @Test
    public void testGetEmail(){
        assertThat(request.getEmail().equals(EMAIL), is(true));
    }

    @Test
    public void testGetToken(){
        assertThat(request.getGoogleToken().equals(TOKEN), is(true));
    }

}
